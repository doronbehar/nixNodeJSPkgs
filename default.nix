# This file is almost identical to:
# <nixpkgs>/pkgs/development/node-packages/default.nix
{ pkgs, nodejs, stdenv, lib }:

let
  since = (version: pkgs.lib.versionAtLeast nodejs.version version);
  before = (version: pkgs.lib.versionOlder nodejs.version version);
  super = import ./composition.nix {
    inherit pkgs nodejs;
    inherit (stdenv.hostPlatform) system;
  };
  self = super // {
    web-ext = super.web-ext.override {
      # Needed since a certain update to web-ext
      buildInputs = [
        super.node-gyp-build
      ];
    };
  };
in self
