{
  description = "NodeJS Packages built for NixOS automatically updated every day!";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self
  , nixpkgs
  , flake-utils
  }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        lib = pkgs.lib;
        # Create a list of attribute sets for each nodejs version, where the
        # attribute names of each attribute set in each element of the list, to
        # include the "nodejs-${ver}/" prefix, and afterwards they are all
        # merged into 1 `packages` attribute set.
        packagesDivided = builtins.map (
          nodejs:
          let
            pkgsForThisNodejs = import ./default.nix {
              inherit pkgs nodejs;
              inherit (pkgs) stdenv lib;
            };
            majorVersion = builtins.elemAt (lib.splitString "." nodejs.version) 0;
            nodejsPrefix = "nodejs-${majorVersion}";
          in
            lib.mapAttrs' (
              name: value: {
                name = "${nodejsPrefix}/${name}";
                inherit value;
            }) pkgsForThisNodejs
        ) [
          pkgs.nodejs_18
          pkgs.nodejs_20
        ];
        # merging the list above is trivial because it's guranteed there will
        # not be identical attribute names.
        packages = lib.attrsets.zipAttrsWith
          (name: values:
            # There should be only 1 value in each list of values for each
            # attribute, due to the attribute names modified above.
            builtins.elemAt values 0
        ) packagesDivided;
      in {
        # NOTE: Due to this wrapped by eachDefaultSystem, this attribute is
        # actually exported as nixNodeJSPkgs.overlay.${system}
        overlay = self: super: packages;
        inherit packages;
        devShell = pkgs.mkShell {
          nativeBuildInputs = [
            pkgs.nodePackages.node2nix
          ];
        };
      }
    );
}
