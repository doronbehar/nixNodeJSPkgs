# Warning: Archived project!

Original motivation for this project (quoting from original README):

> Because I don't trust it to update frequently enough, and the merge conflicts
are a nightmare. See:
>
> - https://github.com/NixOS/nixpkgs/pull/112831#issuecomment-927347836

Rest of original README follows:

-------

# NodeJS Nix packages

An automatically updated (daily) collection of Nix packages. It's flake based,
and it exports node packages that I personally use, and some of them rely upon
`node-gyp`.
